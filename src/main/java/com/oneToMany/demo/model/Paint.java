package com.oneToMany.demo.model;

import com.oneToMany.demo.model.entity.OrderEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
//@AllArgsConstructor
@NoArgsConstructor
public class Paint {
    int orderId;
    String brand;
    int quantity;


    public Paint(int id, String brand, int quantity) {
        this.brand=brand;
        this.orderId=id;
        this.quantity=quantity;
    }
}
