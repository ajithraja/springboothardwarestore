package com.oneToMany.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Door {
    int order_number;
    String type;
    int quantity;

    public Door() {
    }

    public Door(String type, int quantity) {
        this.type = type;
        this.quantity = quantity;
    }
}
