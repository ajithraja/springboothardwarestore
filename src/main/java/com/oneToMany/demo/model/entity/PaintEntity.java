package com.oneToMany.demo.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Getter
@Setter
@Entity(name="paint")
//@AllArgsConstructor
public class PaintEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    int id;

    @NonNull
    @Column(name="brand")
    String brand;
    @NonNull
    @Column(name="quantity")
    int quantity;

    @ManyToOne(optional = false)
    @JoinColumn(name = "orderId")
    @JsonIgnoreProperties("paints")
    OrderEntity orderEntity;

    public PaintEntity() {
    }

    public PaintEntity(int id, @NonNull String brand, @NonNull int quantity) {
        this.id = id;
        this.brand = brand;
        this.quantity = quantity;
    }

    public PaintEntity(@NonNull String brand, @NonNull int quantity) {
        this.brand = brand;
        this.quantity = quantity;
    }
}
