package com.oneToMany.demo.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity(name="door")
@AllArgsConstructor
public class DoorEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    int id;
    @NonNull
    @Column(name="type")
    String type;
    @NonNull
    @Column(name="quantity")
    int quantity;
    @ManyToOne(optional = false)
    @JoinColumn(name = "orderId")
    @JsonIgnoreProperties("doors")
    OrderEntity order;

    public DoorEntity() {
    }

    public DoorEntity(@NonNull String type, @NonNull int quantity) {
        this.type = type;
        this.quantity = quantity;
    }

    public DoorEntity(int id, String type, int quantity) {
        this.id=id;
        this.type=type;
        this.quantity=quantity;
    }
}
