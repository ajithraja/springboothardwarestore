package com.oneToMany.demo.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name="orderTable")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="orderId")
    int orderId;

    @NonNull
    @Column(name="customer_name")
    String customerName;

    @OneToMany(mappedBy = "orderEntity",cascade = CascadeType.ALL)
    @JsonIgnoreProperties("orderEntity")
    List<PaintEntity> paints;

    @OneToMany(mappedBy = "order",cascade = CascadeType.ALL)
    @JsonIgnoreProperties("order")
    List<DoorEntity> doors;

    public OrderEntity(@NonNull String customerName) {
        this.customerName = customerName;
    }
}
