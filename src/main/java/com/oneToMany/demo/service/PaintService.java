package com.oneToMany.demo.service;


import com.oneToMany.demo.Repository.Paint.PaintJpaRepository;
import com.oneToMany.demo.Repository.Paint.PaintRepository;
import com.oneToMany.demo.model.Paint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaintService {

    @Autowired
    PaintRepository paintRepository;
    @Autowired
    PaintJpaRepository paintJpaRepo;

    public List<Paint> getAllPaints()
    {
        return  paintJpaRepo.findAll()
                            .stream()
                            .map(s->{return new Paint(s.getOrderEntity().getOrderId(),s.getBrand(),s.getQuantity());})
                            .collect(Collectors.toList());
    }

    public List<Paint> getPaintByOrderId(int orderId)
    {

       return paintJpaRepo.findByOrderEntity_OrderId(orderId).stream()
                .map(s->{return new Paint(s.getOrderEntity().getOrderId(),s.getBrand(),s.getQuantity());                                })
                .collect(Collectors.toList());
//        return  paintJpaRepo.findAll()
//                            .stream()
//                            .filter(x->x.getOrderEntity().getOrderId()==orderId)
//                            .map(s->{return new Paint(s.getOrderEntity().getOrderId(),s.getBrand(),s.getQuantity());                                })
//                            .collect(Collectors.toList());
    }

    public List<Paint> getPaintByName(String name)
    {
        return paintJpaRepo.findByBrandContaining(name)
                                        .stream()
                                        .map(s->{ return new Paint(s.getOrderEntity().getOrderId(),s.getBrand(),s.getQuantity());})
                                        .collect(Collectors.toList());
    }
}
