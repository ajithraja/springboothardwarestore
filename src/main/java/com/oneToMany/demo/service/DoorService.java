package com.oneToMany.demo.service;


import com.oneToMany.demo.Repository.Door.DoorJpaRepository;
import com.oneToMany.demo.model.Door;
import com.oneToMany.demo.model.entity.DoorEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DoorService {

    @Autowired
    DoorJpaRepository doorJpaRepository;

    public List<Door> getAllDoors() {
        log.info("Inside getAllDoors() service layer");
       return doorJpaRepository.findAll()
                                .stream()
                                .map(s->{return new Door(s.getOrder().getOrderId(),s.getType(),s.getQuantity());})
                                .collect(Collectors.toList());
    }

    public List<Door> getDoorByOrderId(int orderId) {
        log.info("Inside getDoorByOrderId() service layer");

        return doorJpaRepository.findByOrder_OrderId(orderId).stream()
                .map(s->{return new Door(s.getOrder().getOrderId(),s.getType(),s.getQuantity());})
                .collect(Collectors.toList());

//        return doorJpaRepository.findAll()
//                                .stream()
//                                .filter(x->x.getOrder().getOrderId()==orderId)
//                                .map(s->{return new Door(s.getOrder().getOrderId(),s.getType(),s.getQuantity());})
//                                .collect(Collectors.toList());
    }

    public List<Door> getDoorByName(String name) {
        log.info("Inside getDoorByName() service layer");
        return doorJpaRepository.findByTypeContaining(name)
                                .stream()
                                .map(s->{return new Door(s.getOrder().getOrderId(),s.getType(),s.getQuantity());})
                                .collect(Collectors.toList());
    }

}
