package com.oneToMany.demo.service;


import com.oneToMany.demo.Repository.Door.OrderJpaRepository;
import com.oneToMany.demo.model.entity.OrderEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class OrderService {

    @Autowired
    OrderJpaRepository orderRepo;

    public void saveOrder(OrderEntity order)
    {
        log.info("Inside service layer saveOrder()");
        orderRepo.save(order);
    }

    public List<OrderEntity> getAllOrder()
    {
        log.info("Inside service layer getAllOrder()");
        return orderRepo.findAll();
    }

    public Optional<OrderEntity> getById(int id){
        log.info("Inside service layer getById()");
        return orderRepo.findById(id);
    }

    public void deleteById(int id)
    {
        log.info("Inside service layer deleteById()");
        orderRepo.deleteById(id);
    }
}
