package com.oneToMany.demo.ExceptionHandler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value= { ResponseStatusException.class })
    public ResponseEntity<String> customException(ResponseStatusException e)
    {
        log.warn(e.getReason()+" "+e.getRawStatusCode());
        return new ResponseEntity<>( e.getReason() ,e.getStatus());
    }

    @ExceptionHandler(value= { Exception.class })
    public ResponseEntity<String> unexpectedException(Exception e)
    {
        log.error("Exception occurred "+e.getMessage());
        return new ResponseEntity<String>("Exception occurred \n\t"+ e.getMessage(),HttpStatus.BAD_REQUEST);
    }
}
