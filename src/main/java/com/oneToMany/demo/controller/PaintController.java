package com.oneToMany.demo.controller;


import com.oneToMany.demo.model.Paint;
import com.oneToMany.demo.service.PaintService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class PaintController {

    @Autowired
    PaintService paintService;

    @GetMapping("/paints")
    public ResponseEntity<List<Paint>> getAllPaint() throws ResponseStatusException
    {
        log.info("Inside Controller layer getAllPaint()");
        List<Paint> paints=paintService.getAllPaints();
        if(paints.size()>0)
        {
            log.info("Returning list of paint order(s)");
            return new ResponseEntity<List<Paint>>(paints,HttpStatus.OK);
        }
        else
            throw new ResponseStatusException(HttpStatus.NO_CONTENT,"No Paint order to display");
    }

    @GetMapping("/paint/{orderId}")
    public ResponseEntity<List<Paint>> getPaintByOrderId(@PathVariable int orderId) throws ResponseStatusException{
        log.info("Inside Controller layer getPaintByOrderId()");
        List<Paint> paint=paintService.getPaintByOrderId(orderId);
        if(paint.size()==0)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"PaintId not found");
        }
        log.info("Returning list of paint order(s) for the given orderID");
        return new ResponseEntity<>(paint,HttpStatus.FOUND);
    }

    @GetMapping("/paint")
    public ResponseEntity<List<Paint>> getPaintByName(@RequestHeader Map<String, String> headers) {
        log.info("Inside Controller layer getPaintByName()");
        List<Paint> paints=paintService.getPaintByName(headers.get("name"));
        if(paints.size()>0)
        {
            log.info("Returning list of paint order(s) similar to the given name");
            return new ResponseEntity<List<Paint>>(paints, HttpStatus.OK);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"No match found");
    }

}
