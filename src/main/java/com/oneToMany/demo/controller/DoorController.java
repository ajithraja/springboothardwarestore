package com.oneToMany.demo.controller;

import com.oneToMany.demo.model.Door;

import com.oneToMany.demo.service.DoorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class DoorController {

    @Autowired
    DoorService doorService;

    @GetMapping("/doors")
    public ResponseEntity<List<Door>> getAllDoor()
    {
        log.info("Inside getAllDoor() controller layer");
        List<Door> doors=doorService.getAllDoors();
        if(doors.size()>0)
        {
            log.info("Returning list of available door(s) orders");
            return new ResponseEntity<List<Door>>(doors, HttpStatus.OK);
        }
        log.warn("No Door(s) orders found");
        throw new ResponseStatusException(HttpStatus.NO_CONTENT,"No Door Order(s) to display");
    }

    @GetMapping("/door/{orderId}")
    public ResponseEntity<List<Door>> getDoorByOrderId(@PathVariable int orderId) {
        log.info("Inside getDoorByOrderId() controller layer");
        List<Door> door=doorService.getDoorByOrderId(orderId);
        if(door.size()==0)
        {
            log.info("No Door(s) orders found for the given OrderID");
            return new ResponseEntity<>(door,HttpStatus.NOT_FOUND);
        }
        log.warn("Returning list of available door(s) orders for the given OrderID");
        return new ResponseEntity<>(door,HttpStatus.FOUND);
    }

    @GetMapping("/door")
    public ResponseEntity<List<Door>> getDoorByType(@RequestHeader Map<String, String> headers) {
        log.info("Inside getDoorByType() controller layer");
        List<Door> doors=doorService.getDoorByName(headers.get("type"));
        if(doors.size()>0){
            log.info("No Door(s) orders found similar to the given name");
            return new ResponseEntity<List<Door>>(doors, HttpStatus.OK);
        }

        log.info("Door(s) orders found similar to the given name");
        return  new ResponseEntity<List<Door>>(new ArrayList<>(), HttpStatus.NO_CONTENT);

    }


}
