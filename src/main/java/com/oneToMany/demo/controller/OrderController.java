package com.oneToMany.demo.controller;


import com.oneToMany.demo.model.entity.DoorEntity;
import com.oneToMany.demo.model.entity.OrderEntity;
import com.oneToMany.demo.model.entity.PaintEntity;
import com.oneToMany.demo.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
public class OrderController {
    @Autowired
    OrderService orderService;

    @GetMapping("/start")
    public OrderEntity startUp()
    {
        PaintEntity p1=new PaintEntity("asian",12);
        PaintEntity p2=new PaintEntity("berlax",3);

        DoorEntity d1=new DoorEntity("single",2);
        DoorEntity d2=new DoorEntity("Double",5);

        OrderEntity o1=new OrderEntity("Ajith");
        List<PaintEntity> paintList=new ArrayList<>();
        List<DoorEntity> doorList=new ArrayList<>();

        d1.setOrder(o1);
        d2.setOrder(o1);

        p1.setOrderEntity(o1);
        p2.setOrderEntity(o1);

        paintList.add(p1);
        paintList.add(p2);

        doorList.add(d1);
        doorList.add(d2);

        o1.setDoors(doorList);
        o1.setPaints(paintList);

        orderService.saveOrder(o1);
        return o1;
    }
    @GetMapping("/order")
    public ResponseEntity<List<OrderEntity>> getAllOrders() throws ResponseStatusException
    {
        log.info("Inside getAllOrders() controller layer");
        List<OrderEntity> orders=orderService.getAllOrder();
        if(orders.size()>0)
        {
            log.info("Returning list of available order(s)");
            return new ResponseEntity<>(orders, HttpStatus.OK);
        }

            throw new ResponseStatusException(HttpStatus.NO_CONTENT,"No Order(s) to display");
    }

    @GetMapping("/order/{id}")
    public ResponseEntity<OrderEntity> getOrderById(@PathVariable int id)
    {
        log.info("Inside getOrderById() controller layer");
        Optional<OrderEntity> order=orderService.getById(id);
        if(order.isPresent())
        {
            log.info("Returning list of available order for the given id");
            return new ResponseEntity<>(order.get(),HttpStatus.FOUND);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"OrderId not found");
    }

    @PostMapping("/order")
    public ResponseEntity<OrderEntity> addOrder(@RequestBody OrderEntity newOrder)
    {
        log.info("Inside addOrder() controller layer");
        orderService.saveOrder(newOrder);
        return new ResponseEntity<>(newOrder,HttpStatus.OK);
    }

    @PutMapping("/order")
    public ResponseEntity<OrderEntity> updateOrderById(@RequestBody OrderEntity order)
    {
        log.info("Inside updateOrderById() controller layer");
        if(orderService.getById(order.getOrderId())!=null) {
            orderService.saveOrder(order);
            return new ResponseEntity<>(order,HttpStatus.OK);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"OrderId not found");
    }

    @DeleteMapping("/order/{id}")
    public void deleteOrderById(@PathVariable int id) throws ResponseStatusException
    {
        log.info("Inside deleteOrderById() controller layer");

        if(!orderService.getById(id).isPresent()) {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"OrderId not found");
        }
        orderService.deleteById(id);
    }

}
