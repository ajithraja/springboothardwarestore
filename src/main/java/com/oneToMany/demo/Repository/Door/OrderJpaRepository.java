package com.oneToMany.demo.Repository.Door;


import com.oneToMany.demo.model.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderJpaRepository extends JpaRepository<OrderEntity,Integer> {
}
