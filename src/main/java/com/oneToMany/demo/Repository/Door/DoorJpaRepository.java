package com.oneToMany.demo.Repository.Door;



import com.oneToMany.demo.model.entity.DoorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoorJpaRepository extends JpaRepository<DoorEntity,Integer> {
    List<DoorEntity> findByTypeContaining(String type);
    List<DoorEntity> findByOrder_OrderId(int orderId);
}
