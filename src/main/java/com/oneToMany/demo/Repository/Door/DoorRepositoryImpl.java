package com.oneToMany.demo.Repository.Door;


import com.oneToMany.demo.model.Door;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class DoorRepositoryImpl implements DoorRepository{
    final List<Door> doors;
    public DoorRepositoryImpl()
    {
        doors=new ArrayList<>();
        doors.add(new Door(201,"Single",15));
        doors.add(new Door(202,"Double",13));
    }
    @Override
    public List<Door> getDoor() {
        return doors;
    }

    @Override
    public Door getDoorById(int id) {
        return doors.stream().filter(x->x.getOrder_number()==id).findFirst().orElse(null);
    }

    @Override
    public List<Door> getDoorByType(String name) {
        return doors.stream().filter(x->x.getType().startsWith(name)).collect(Collectors.toList());
    }

    @Override
    public void addNewDoor(Door newDoor) {
        doors.add(newDoor);
    }

    @Override
    public void updateDoor(Door Door) {
        doors.stream().forEach(x->{
            if(x.getOrder_number()==Door.getOrder_number())
                x.setType(Door.getType());
            x.setQuantity(Door.getQuantity());
        });
    }

    @Override
    public void deleteDoor(int id) {
        doors.removeIf(x->x.getOrder_number()==id);
    }
}
