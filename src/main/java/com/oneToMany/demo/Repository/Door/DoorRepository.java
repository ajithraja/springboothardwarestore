package com.oneToMany.demo.Repository.Door;


import com.oneToMany.demo.model.Door;

import java.util.List;

public interface DoorRepository {
    public List<Door> getDoor();
    public Door getDoorById(int id);
    public List<Door> getDoorByType(String name);
    public void addNewDoor(Door newDoor);
    public void updateDoor(Door Door);
    public void deleteDoor(int id);
}
