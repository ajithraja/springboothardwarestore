package com.oneToMany.demo.Repository.Paint;


import com.oneToMany.demo.model.Paint;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PaintRepositoryImpl implements PaintRepository {

    final List<Paint> paints;
    public PaintRepositoryImpl()
    {
        paints=new ArrayList<>();
        paints.add(new Paint(101,"Berlax",20));
        paints.add(new Paint(102,"Asian",18));
        paints.add(new Paint(103,"Natural",35));
    }

    @Override
    public List<Paint> getPaint() {
        return paints;
    }

    @Override
    public Paint getPaintById(int id) {
        return paints.stream().filter(x->x.getOrderId()==id).findFirst().orElse(null);
    }

    @Override
    public List<Paint> getPaintByName(String name) {
        return paints.stream().filter(x->x.getBrand().startsWith(name)).collect(Collectors.toList());
    }

    @Override
    public void addNewPaint(Paint newPaint) {

        paints.add(newPaint);

    }

    @Override
    public void updatePaint(Paint paint) {
        paints.stream().forEach(x->{
            if(x.getOrderId()==paint.getOrderId())
            x.setBrand(paint.getBrand());
            x.setQuantity(paint.getQuantity());
        });
    }

    @Override
    public void deletePaint(int id) {
        paints.removeIf(x->x.getOrderId()==id);
    }
}
