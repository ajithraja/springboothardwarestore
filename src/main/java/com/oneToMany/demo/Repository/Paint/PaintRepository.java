package com.oneToMany.demo.Repository.Paint;



import com.oneToMany.demo.model.Paint;

import java.util.List;

public interface PaintRepository {
    public List<Paint> getPaint();
    public Paint getPaintById(int id);
    public List<Paint> getPaintByName(String name);
    public void addNewPaint(Paint newPaint);
    public void updatePaint(Paint paint);
    public void deletePaint(int id);
}
