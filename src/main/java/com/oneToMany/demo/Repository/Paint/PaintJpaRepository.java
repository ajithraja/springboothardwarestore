package com.oneToMany.demo.Repository.Paint;


import com.oneToMany.demo.model.entity.PaintEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaintJpaRepository extends JpaRepository<PaintEntity,Integer> {
    List<PaintEntity> findByBrandContaining(String brand);
    List<PaintEntity> findByOrderEntity_OrderId(int OrderId);


}
